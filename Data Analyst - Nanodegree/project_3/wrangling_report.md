## Project 3 - WeRateDogs Twitter

### Step 1: Gathering data

The code of my `run.py`

```python
import sys

sys.path.append("/Users/arni/projects/py_functions")
import re

import assess
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import requests
import seaborn as sns

import clean

"""
# 1. Gather

# Load the file twitter-archive-enhanced.csv into a df
df = pd.read_csv("twitter-archive-enhanced.csv")
print(df.head())

# Load the file image_predictions.tsv from Udacity into a df
image_data_df = gather.load_url_file(
    "https://d17h27t6h515a5.cloudfront.net/topher/2017/August/599fd2ad_image-predictions/image-predictions.tsv",
    "image-predictions.tsv",
    "\t",
)
print(image_data_df.head())

# Load data with the Twitter API
twitter_json_df = gather.twitter_api(
    "YU81pIm8RYnMfKMs3VtFwd7YO",
    "7qZq4J5CG6ryE13jOPC2aKafP2he6CoW9PgrLJx3lgePcRSNPU",
    "627133571-Jiha7HwRcy9h9XvZqx4bQgpknUAmUtU9ZQehguqd",
    "buxxhNfubs3VR1uO3kCKmsWnPLKPit5fVILQVlNF7WDey",
    df,
)
print(twitter_json_df.head())

"""

# 2. Assess

twitter_archive_enhanced_df = pd.read_csv("twitter-archive-enhanced.csv")

image_data_df = pd.read_csv("image-predictions.tsv", sep="\t")

twitter_json_df = pd.read_csv("tweet_json.txt")

dataframes = {
    "twitter-archive-enhanced": twitter_archive_enhanced_df,
    "image_data_df": image_data_df,
    "twitter_json_df": twitter_json_df,
}

for key, df in dataframes.items():
    print(f"\n\n______ {key} ______")
    print("\nVISUAL ASSESSMENT")
    assess.visual(df, key)
    print("\nPROGRAMMATIC ASSESSMENT")
    assess.programmatic(df, key)

# 3. Clean

twitter_archive_enhanced_df_clean = twitter_archive_enhanced_df.copy()
image_data_df_clean = image_data_df.copy()
twitter_json_df_clean = twitter_json_df.copy()

dataframes_clean = {
    "twitter-archive-enhanced": twitter_archive_enhanced_df_clean,
    "image_data_df": image_data_df_clean,
    "twitter_json_df": twitter_json_df_clean,
}

print("\n____________________________________________________________________")
# Define 1

print("\nIssue 1 - Remove columns with with too many missing values\n")

# Code 1
twitter_archive_enhanced_df_clean.drop(
    [
        "in_reply_to_status_id",
        "in_reply_to_user_id",
        "retweeted_status_id",
        "retweeted_status_timestamp",
    ],
    axis=1,
    inplace=True,
)

# Test 1
print(twitter_archive_enhanced_df_clean.info())

print("\n____________________________________________________________________")
# Define 2
print("\nIssue 2 - Clean sources column and make it more readable\n")

# Code 2
twitter_archive_enhanced_df_clean.source = (
    twitter_archive_enhanced_df_clean.source.apply(
        lambda x: re.findall(r">(.*)<", x)[0]
    )
)

# Test 2
print(twitter_archive_enhanced_df_clean.source.value_counts())

print("\n____________________________________________________________________")
# Define 3
print(
    "\nIssue 3 - Keep only original ratings that have images \
    (exclude retweets)\n"
)

# Code 3
print(sum(twitter_archive_enhanced_df_clean.retweeted_status_user_id.value_counts()))

twitter_archive_enhanced_df_clean = twitter_archive_enhanced_df_clean[
    pd.isnull(twitter_archive_enhanced_df_clean["retweeted_status_user_id"])
]

# Test 3
print(sum(twitter_archive_enhanced_df_clean.retweeted_status_user_id.value_counts()))

print("\n____________________________________________________________________")

# Define 4
print("\nIssue 4 - Separate the timestamp into atomic data day, month and year\n")

# Code 4
pd.to_datetime(twitter_archive_enhanced_df_clean.timestamp)

twitter_archive_enhanced_df_clean["Year"] = pd.to_datetime(
    twitter_archive_enhanced_df_clean["timestamp"]
).dt.strftime("%Y")
twitter_archive_enhanced_df_clean["Month"] = pd.to_datetime(
    twitter_archive_enhanced_df_clean["timestamp"]
).dt.strftime("%M")
twitter_archive_enhanced_df_clean["Day"] = pd.to_datetime(
    twitter_archive_enhanced_df_clean["timestamp"]
).dt.strftime("%d")

# Finally drop timestamp column
twitter_archive_enhanced_df_clean = twitter_archive_enhanced_df_clean.drop(
    "timestamp", 1
)

# Test 4
print(twitter_archive_enhanced_df_clean.head(5))
print(twitter_archive_enhanced_df_clean.columns)

print("\n____________________________________________________________________")

# Define 5
print(
    "\nIssue 5 - Merge the columns doggo, floofer, pupper and puppo \
    to one column without the 'None'\n"
)

# Code 5
twitter_archive_enhanced_df_clean["DOG_STAGE"] = (
    twitter_archive_enhanced_df_clean["doggo"].str.lstrip("None")
    + twitter_archive_enhanced_df_clean["floofer"].str.lstrip("None")
    + twitter_archive_enhanced_df_clean["pupper"].str.lstrip("None")
    + twitter_archive_enhanced_df_clean["puppo"].str.lstrip("None")
)

twitter_archive_enhanced_df_clean = twitter_archive_enhanced_df_clean.drop(
    [
        "doggo",
        "floofer",
        "pupper",
        "puppo",
    ],
    1,
)

# Test 5
print(twitter_archive_enhanced_df_clean.head(5))
print(twitter_archive_enhanced_df_clean["DOG_STAGE"].value_counts())

print("\n____________________________________________________________________")

# Define 6
print("\nIssue 46 - Create columns for image prediction and confidence level\n")

# Code 6
dog_race = []
confidence = []


def get_dog_race_confidence(dataframe):
    if dataframe["p1_dog"] is True:
        dog_race.append(dataframe["p1"])
        confidence.append(dataframe["p1_conf"])
    elif dataframe.p2_dog is True:
        dog_race.append(dataframe["p2"])
        confidence.append(dataframe["p2_conf"])
    elif dataframe.p3_dog is True:
        dog_race.append(dataframe["p3"])
        confidence.append(dataframe["p3_conf"])
    else:
        dog_race.append("NaN")
        confidence.append(0)


image_data_df_clean.apply(get_dog_race_confidence, axis=1)
image_data_df_clean["dog_race"] = dog_race
image_data_df_clean["confidence"] = confidence

image_data_df_clean = image_data_df_clean.drop(
    [
        "img_num",
        "p1",
        "p1_conf",
        "p1_dog",
        "p2",
        "p2_conf",
        "p2_dog",
        "p3",
        "p3_conf",
        "p3_dog",
    ],
    1,
)


# Test 6
print(image_data_df_clean.info())

print("\n____________________________________________________________________")

# Define 7
print("\nIssue 7 - Find the 66 duplicate images and drop them \n")

# Code 7
image_data_df_clean = image_data_df_clean.drop_duplicates(
    subset=["jpg_url"], keep="last"
)

# Test 7
print(sum(image_data_df_clean["jpg_url"].duplicated()))

print("\n____________________________________________________________________")

# Define 8
print("\nIssue 9 - Keep only original tweets")

# Code 8
tweet_json_clean = twitter_json_df_clean[
    twitter_json_df_clean["retweet_status"] == "Original tweet"
]

# Test 8
twitter_json_df_clean["retweet_status"].value_counts()

print("\n____________________________________________________________________")

# Define 9
print("\nIssue 11 - Combine the three dataframes into one master data set\n")

# Code 9
twitter_archive_master = pd.merge(
    twitter_archive_enhanced_df_clean, image_data_df_clean, on=["tweet_id"]
)
twitter_archive_master = pd.merge(
    twitter_archive_master, twitter_json_df_clean, on="tweet_id"
)

# Test 11
print(twitter_archive_master.head(5))

print("\n____________________________________________________________________")

# Define 9 & 10
print(
    "\nIssue 12 and 12 - Set all column titles to UPPERCASE, \
    Remove possible whitespace from column titles\n"
)

# Code 9 & 10
clean.clean_header(twitter_archive_master)

# Test 9 & 10
print(twitter_archive_master.head(1))

print("\n____________________________________________________________________")

```

The code of my `gather.py` file

```python
import json
import pandas as pd
import requests
import tweepy
from tweepy import OAuthHandler


def load_url_file(url, file_name, sep):
    data = requests.get(url)
    with open(url.split("/")[-1], mode="wb") as file:
        file.write(data.content)
    return pd.read_csv(file_name, sep)


def twitter_api(consumer_key, consumer_secret, access_token,
                access_secret, id_source):
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)

    api = tweepy.API(auth, wait_on_rate_limit=True)

    """
    NOTE TO STUDENT WITH MOBILE VERIFICATION ISSUES:
    df_1 is a DataFrame with the twitter_archive_enhanced.csv file. \
    You may have to change line 17 to match the name of your DataFrame \
    with twitter_archive_enhanced.csv
    NOTE TO REVIEWER: \
    this student had mobile verification issues so the following
    Twitter API code was sent to this student from a Udacity instructor
    Tweet IDs for which to gather additional data via Twitter's API
    """

    tweet_ids = id_source.tweet_id.values
    len(tweet_ids)

    # Query Twitter's API for JSON data for each tweet ID
    # in the Twitter archive
    count = 0
    fails_dict = {}

    # Save each tweet's returned JSON as a new line in a .txt file
    with open("tweet_json.txt", "w") as outfile:
        # This loop will likely take 20-30 minutes to run
        # because of Twitter's rate limit
        for tweet_id in tweet_ids:
            count += 1
            print(str(count) + ": " + str(tweet_id))
            try:
                tweet = api.get_status(tweet_id, tweet_mode="extended")
                print("Success")
                json.dump(tweet._json, outfile)
                outfile.write("\n")
            except tweepy.TweepError as e:
                print("Fail")
                print(e.api_code)
                print(e)
                fails_dict[tweet_id] = e
                pass

    temp = []

    with open("tweet_json.txt", encoding="utf-8") as js_file:
        for i in js_file:
            data = json.loads(i)
            tweet_id = data["id"]
            tweet = data["full_text"]
            url1 = tweet[tweet.find("https"):]
            favorite_count = data["favorite_count"]
            retweet_count = data["retweet_count"]
            retweet_status = data["retweet_status"] = data.get(
                "retweet_status", "Original tweet"
            )
            if retweet_status == "Original tweet":
                url = url1
            else:
                retweet_status = "A retweet"
                url = "A retweet"

            temp.append(
                {
                    "tweet_id": tweet_id,
                    "favorite_count": favorite_count,
                    "retweet_status": retweet_status,
                    "url": url,
                    "retweet_count": retweet_count,
                }
            )

    # create a new DataFrame
    df_from_json = pd.DataFrame(
        temp,
        columns=[
            "tweet_id",
            "favorite_count",
            "retweet_count",
            "retweet_status",
            "url",
        ],
    )

    df_from_json.to_csv("tweet_json.txt", index=False)

    return df_from_json

```

### Step 2: Assessing Data
After gathering all three pieces of data, assess them visually and programmatically for quality and tidiness issues. Detect and document at least eight (8) quality issues and two (2) tidiness issues in the "Accessing Data" section in the wrangle_act.ipynb Jupyter Notebook.

You need to use two types of assessment:

Visual assessment: each piece of gathered data is displayed in the Jupyter Notebook for visual assessment purposes. Once displayed, data can additionally be assessed in an external application (e.g. Excel, text editor).
Programmatic assessment: pandas' functions and/or methods are used to assess the data.

The code of my `assess.py` file


```python
from pandas.api.types import is_numeric_dtype
from pandas.api.types import is_object_dtype

import numpy as np


def visual(dataframe, string_name):
    print("\nA. Head from " + string_name)
    print(dataframe.head())
    print("\nB. Tail from " + string_name)
    print(dataframe.tail())
    print("\nC. Sample of 25 rows from " + string_name)
    df_elements = dataframe.sample(n=25)
    print(df_elements.head(25))


def programmatic(dataframe, string_name):
    print(f"\nD. Shape from {string_name}:")
    print(dataframe.shape)
    print(f"\nE. Info from {string_name}:")
    print(dataframe.info())
    print(f"\nF. isnull sum from {string_name}:")
    print(dataframe.isnull().sum())
    print(f"\nG. Duplicates from {string_name}:")
    print(sum(dataframe.duplicated()))
    print(f"\nH. Describe from {string_name}:")
    print(dataframe.describe())
    print(f"\nI. Nunique from {string_name}:")
    print(dataframe.nunique())

    print(f"\nJ. Duplicates per column from {string_name}:")
    for i, col in zip(dataframe.dtypes, dataframe):
        print(
            f"Sum of duplicates in {col} = "
            + str(sum(dataframe[col].duplicated()))
            + f"({i})"
        )

    print(f"\nK. Sorted numeric values per column in {string_name}:")
    for i, col in zip(dataframe.dtypes, dataframe):
        if is_numeric_dtype(
            dataframe[col]
        ):
            print(np.sort(dataframe[col].unique()))
        else:
            print(col + " is not numeric")

    print(f"\nL. Check if column is object from {string_name}:")
    for i, col in zip(dataframe.dtypes, dataframe):
        if is_object_dtype(dataframe[col]):
            print(str(col) + str(i))
        else:
            pass

    print(f"\nM. Count values per column from {string_name}:")
    c = 1
    for i, col in zip(dataframe.dtypes, dataframe):
        print("\n" + str(c) + ". " + col)
        print(dataframe[col].value_counts())
        c = c + 1

```

### Step 3: Cleaning data


The code of my `clean.py` file

```python
def clean_header(dataframe):
    dataframe.columns = dataframe.columns.str.strip()
    dataframe.columns = dataframe.columns.str.upper()


def get_dog_race_confidence(dataframe):
    dog_race = []
    confidence = []

    if dataframe.p1_dog is True:
        dog_race.append(dataframe["p1"])
        confidence.append(dataframe["p1_conf"])
    elif dataframe.p2_dog is True:
        dog_race.append(dataframe["p2"])
        confidence.append(dataframe["p2_conf"])
    elif dataframe.p3_dog is True:
        dog_race.append(dataframe["p3"])
        confidence.append(dataframe["p3_conf"])
    else:
        dog_race.append("NaN")
        confidence.append(0)

    return dog_race
    return confidence

```


**twitter_archive_enhanced_df**
Quality

Tidiness
 - 1. Remove columns with with too many missing values (`in_reply_to_status_id` , `in_reply_to_user_id`, `retweeted_status_user_id`, `retweeted_status_timestamp`)
 - 2. Clean sources column and make it more readable
 - 3. Keep only original ratings that have images (exclude retweets)
 - 4. Separate the timestamp into atomic data day, month and year
 - 5. Merge the columns doggo, floofer, pupper and puppo to one column

**image_data_df**
Quality

Tidiness
 - 6. Create columns for image prediction and confidence level from `p1`, `p1_conf`, `p1_dog`, `p2`, `p2_conf`, `p2_dog`, `p3`, `p3_conf`, `p3_dog`
 - 7. Clean the dog race name. Change '_' to whitespace and set the first letter capital
 - 8. Find the 66 duplicate images and drop them (`tweet_id    2075`, `jpg_url     2009`)

**twitter_json_df**

Quality

Tidiness
 - 9. Keep only original tweets

 **twitter_archive_master_df**
 - 10. Combine the three dataframes into one master data set
 - 11. Set all column titles to UPPERCASE
 - 12. Remove possible whitespace from column titles

### Step 4: Storing data

```python
twitter_archive_master.to_csv(
    './resources/twitter_archive_master.csv',
    encoding='utf-8',
    index=False
```

### Step 5: Analyzing, and visualizing data



### Step 6: Reporting
